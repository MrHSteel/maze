const playerCoordinate = {
    row: 0,
    cell: 0
}

const map = [
    "WWWWWWWWWWWWWWWWWWWWWWWWWWWFWWWWW",
    "W    WW    WW W     W        W WW",
    "W WW  W WW W  W W W W   WW W W  W",
    "W  WW    W W WWWW WW     W W WW W",
    "WW  W WW WWW W        WW W W W  W",
    "WWW WWWW W W   WWWWWW WW W   W WW",
    "S     WWWW   W WW   W W  W W W  W",
    "W WWW      W      W WWWWWW WWWW W",
    "W   W WWWW   WWWW W   W  W W W  W",
    "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW",
];

const mapGrid = document.getElementById('mapGrid')

//builds map
for (let rowIndex = 0; rowIndex < map.length; rowIndex++) {
    let mapRow = map[rowIndex]
    let row = document.createElement('div')
    row.dataset.rowIndex = rowIndex
    row.className = 'row'

    for (let cellIndex = 0; cellIndex < mapRow.length; cellIndex++) {
        let cell = document.createElement('div')
        cell.dataset.cellIndex = cellIndex
        cell.dataset.rowIndex = rowIndex
        cell.className = 'cell'

        switch (mapRow[cellIndex]) {
            case 'W': cell.classList.add('wall')
            break
            case ' ': cell.classList.add('space')
            break
            case 'S': 
                cell.id = 'start'; 
                playerCoordinate.row = rowIndex; 
                playerCoordinate.cell = cellIndex;
            break
            case 'F': cell.classList.add('finish')
            break
        }
        row.appendChild(cell)
    }
    mapGrid.appendChild(row)
}


//moves the player
function movePlayer (row, cell) {
    
    if (map[row][cell] == 'F'){
        alert('You win')
    } else if (!map[row] || map[row][cell] !== ' '){
        return
    }

    movePlayerModel(row, cell)
    movePlayerView(row, cell)
}

//shows where the player is
function movePlayerModel (row, cell) {
    playerCoordinate.row = row
    playerCoordinate.cell = cell
}

//moves the player piece visually
function movePlayerView (row, cell) {
    const nextCell = document.querySelector("[data-row-index='" + row + "'][data-cell-index='" + cell + "']")
    nextCell.appendChild(player)
}

//adds keydown events
document.addEventListener('keydown', event => {
    if (!event.key.startsWith('Arrow')) return

    const directions = {
        ArrowLeft: [playerCoordinate.row, playerCoordinate.cell - 1],
        ArrowRight: [playerCoordinate.row, playerCoordinate.cell + 1],
        ArrowDown: [playerCoordinate.row + 1, playerCoordinate.cell],
        ArrowUp: [playerCoordinate.row - 1, playerCoordinate.cell],
    }

    movePlayer(...directions[event.key])

    console.log({ row: playerCoordinate.row, cell: playerCoordinate.cell })
})

//finds the player piece
function playerPiece() {
    let startCell = document.querySelector('#start')
    const player = document.getElementById('player')
    startCell.appendChild(player)

    return player
}

player = playerPiece()

